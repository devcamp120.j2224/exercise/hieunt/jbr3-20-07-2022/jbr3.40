package com.jbr340.circlerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CirclerestapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CirclerestapiApplication.class, args);
		// CircleController.getCircleArea(3.21);
	}

}

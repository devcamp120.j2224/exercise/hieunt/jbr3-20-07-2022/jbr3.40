package com.jbr340.circlerestapi;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import org.springframework.web.bind.annotation.RequestParam;


@RestController
public class CircleController {
    @CrossOrigin
    @GetMapping("/circle-area")
    public static double getCircleArea(@RequestParam(required = true) double radius) {
        Circle circle = new Circle(radius);
        System.out.println(circle);
        return circle.getArea();
    }
    
}
